from tortoise import Tortoise, run_async # pylint: disable=no-name-in-module


async def initialization():
    db_url = "sqlite://db.sqlite"

    await Tortoise.init(db_url=db_url, modules={"models": ["models"]})
    await Tortoise.generate_schemas()

if __name__ == "__main__":
    run_async(initialization())
