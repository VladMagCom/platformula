from tortoise.models import Model
from tortoise.fields import CharField, IntField, DateField
from tortoise.fields import ForeignKeyRelation as FKR
from tortoise.fields import ForeignKeyField as FK
from tortoise.fields import RESTRICT
from tortoise.fields import OneToOneRelation as O2OR
from tortoise.fields import OneToOneField as O2OField
from tortoise.fields import ManyToManyField as M2MField
from tortoise.fields import ManyToManyRelation as M2MR


class User(Model):
    id = IntField(pk=True)
    name = CharField(max_length=16, null=True)
    last_name = CharField(max_length=16, null=True)
    username = CharField(max_length=16, null=True)
    # TODO: new class adress
    adress = CharField(max_length=126, null=True)
    email = CharField(max_length=32, unique=True, null=True)
    telegram_id = CharField(max_length=32, unique=True, null=True)


class Customer(Model):
    user: O2OR[User] = O2OField(
        "models.User",
        related_name="customer",
        on_delete=RESTRICT,
    )
    registered_by: O2OR["Admin"] = O2OField("models.Admin", related_name="customers")


class Admin(Model):
    user: O2OR[User] = O2OField(
        "models.User",
        related_name="admin",
        on_delete=RESTRICT,
    )


class Client(Model):
    user: O2OR[User] = O2OField(
        "models.User",
        related_name="client",
        on_delete=RESTRICT,
    )


class Store(Model):
    id = IntField(pk=True)
    name = CharField(max_length=32, unique=True, index=True)
    category: FKR["StoreCategory"] = FK("models.StoreCategory", related_name="stores")
    created_by: FKR[Admin] = FK("models.Admin", related_name="stores")
    customers: M2MR[Customer] = M2MField("models.Customer", related_name="stores")


class StoreCategory(Model):
    id = IntField(pk=True)
    name = CharField(max_length=32, unique=True)


class Product(Model):
    id = IntField(pk=True)
    name = CharField(max_length=64)
    category: FKR["ProductCategory"] = FK(
        "models.ProductCategory",
        related_name="products",
    )


class ProductCategory(Model):
    id = IntField(pk=True)
    name = CharField(max_length=32, unique=True, index=True)
    prototypes: M2MR["ParameterPrototype"] = M2MField(
        "models.ParameterPrototype", related_name="categories"
    )


class ProductParameter(Model):
    prototype: FKR["ParameterPrototype"] = FK(
        "models.ParameterPrototype", related_name="values"
    )
    value = CharField(max_length=64)
    product: FKR[Product] = FK("models.Product", related_name="parameters")


class ParameterPrototype(Model):
    id = IntField(pk=True)
    name = CharField(max_length=32, unique=True)


class Shopping_Cart(Model):
    id = IntField(pk=True)
    client: FKR[Client] = FK("models.Client", related_name="carts")
    products: M2MR[Product] = M2MField("models.Product", related_name="carts")


class OrderForm(Model):
    id = IntField(pk=True)


class Order(Model):
    cart: O2OR[Shopping_Cart] = O2OField("models.Shopping_Cart", related_name="order")
    client: FKR[Client] = FK("models.Client", related_name="orders")
    form: FKR[OrderForm] = FK("models.OrderForm", related_name="orders")


class PaidOrder(Model):
    order: O2OR[Order] = O2OField(
        "models.Order", related_name="paid", on_delete=RESTRICT
    )


class CompletedOrder(Model):
    paid_order: O2OR[PaidOrder] = O2OField(
        "models.PaidOrder",
        related_name="completed",
        on_delete=RESTRICT,
    )
    customer: FKR[Customer] = FK(
        "models.Customer",
        related_name="completed_orders",
    )
    date = DateField()
